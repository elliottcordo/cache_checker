#!/usr/bin/env python3
"""
simple redis cache checker
"""
import configparser
from flask import Flask, render_template, request
import redis

CONFIG = configparser.ConfigParser()
REDIS_HOST = ''
DB_NO = ''

try:
    CONFIG.read('checker.cfg')
    REDIS_HOST = CONFIG['checker']['redis_host']
    DB_NO = CONFIG['checker']['db_no']
except:
    print("config issues")

APP = Flask(__name__)


@APP.route('/')
def m2e():
    """
    main entrypoint
    :return:
    """
    return render_template('input.html', Host=REDIS_HOST, DB=DB_NO)


@APP.route('/result', methods=['POST', 'GET'])
def result():
    """
    render result
    :return:
    """
    results = None
    header = None
    if request.method == 'POST':
        form_data = request.form
        rconn = redis.Redis(host=form_data['Host'], port=6379, db=form_data['DB'])
        key = form_data['Key']
        # try to find some data in redis
        try:
            results = rconn.hgetall(key)
            header = 'hset: ' + key
        except:
            pass
        if not results:
            try:
                val = rconn.get(key)
                results = {val: b''} if val else None
                header = 'key-value: ' + key
            except:
                pass
        # return results
        if not results:
            return render_template("not_found.html")
        else:
            return render_template("result.html", result=results, header=header)
        rconn.connection_pool.disconnect()


if __name__ == '__main__':
    APP.run(debug=True,host='0.0.0.0')
